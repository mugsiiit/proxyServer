#Proxy Server code
import socket, sys, os
from thread import *
import operator

count=0
dict1={}
cacheDict={}

try:
    listening_port = 12345
except KeyboardInterrupt:
    print "\nUser Requested an interrupt"
    print "Application Exiting ..."
    sys.exit()

max_conn = 5
buffer_size = 40960
def main():
    try:
        # print count
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind(('',listening_port))
        #print "\nSocket Created and binded"
        s.listen(max_conn)
    except Exception, e:
        print "\nUnable to initialize socket"
        sys.exit(2)
    while 1:
        try:
            conn, addr = s.accept()
            print "Connection with client established"
            print "dict1ening to host: "+ str(conn)
            data = conn.recv(buffer_size)
            print data
            #first_line = data.split('\n')[0]
            #print "fl: "+first_line
            #if(first_line != "GET http:/// HTTP/1.1"):
            #print "Request: ",data
            start_new_thread(parse_str, (conn, data, addr))
                #parse_str(conn, data, addr)
        except KeyboardInterrupt:
            print "\nProxy Server shutting down ..."
            sys.exit(1)
    s.close()

def parse_str(conn, data, addr):
    try:
        first_line = data.split('\n')[0]
        #print "First Line" + first_line
        url = first_line.split(' ')[1]
        #print "url" + url
        http_pos = url.find("://")
        if(http_pos == -1):
            temp = url
        else:
            temp = url[(http_pos+3):]
        #print "temp: "+ temp
        port_pos = temp.find(":")
        webserver_pos = temp.find("/")
        if(webserver_pos == -1):
            webserver_pos = len(temp)
        webserver = ""
        port = -1
        if(port_pos == -1 or webserver_pos < port_pos):
            port = 80
            webserver = temp[:webserver_pos]
        else:
            port = int((temp[(port_pos+1):])[:webserver_pos-port_pos-1])
            webserver = temp[:port_pos]
        #print "Port: " + str(port)
        #print "Webserver: "+ webserver
        path = temp[webserver_pos+1:]
        proxy_server(webserver, port, conn, addr, data, path)
    except Exception, e:
        pass

def proxy_server(webserver, port, conn, addr, data, path):
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.connect((webserver,port))
        serverReq = "GET /%s HTTP/1.1\r\nHost: %s\r\n" %(path,webserver)
        print "GET /%s HTTP/1.1\r\nHost: %s\r\n\r\n" %(path,webserver)
        # s.send("GET /%s HTTP/1.1\r\nHost: %s\r\n\r\n" %(path,webserver))
        caching(path, s,conn, serverReq)
        s.close()
        conn.close()
    except socket.error, (value, message):
        s.close()
        conn.close()
        sys.exit(1)

def caching(path, s,conn, serverReq):
    global count
    global dict1
    global cacheDict
    flag = 0
    try:
        dict1[path] = dict1[path] + 1
    except KeyError:
        dict1[path] = 1
    for key in cacheDict.keys():
        cacheDict[key] = dict1[key]
    print "Updating Dict1 before caching: ",dict1
    print "Updating cachedict before caching: ",cacheDict
    try:
        print "found in cache"
        with open(path) as fp:
            for i,line in enumerate(fp):
                if line[0] == 'D':
                    print "printing extracted date: ",line[6:]
                    serverReq1 = serverReq+"If-Modified-Since: %s\r\n\r\n" %(line[6:])
                    break
        fp.close()
        s.send(serverReq1)
        reply = s.recv(buffer_size)
        response_code = int(reply.split(' ')[1])
        print "Response code: ",response_code
        print "Reply :", reply
        if response_code == 200:
            try:
                os.remove(path)
            except Exception,e:
                print "Couldnt remove exisitng file"
            file1 = open(path,"w")
            serverReq = serverReq + "\r\n"
            print "server request sending: ", serverReq
            s.send(serverReq)
            print "printing reply on modification"
            # reply1 = s.recv(buffer_size)
            # print reply1
            while 1:
                print "Reached here"
                reply2 = s.recv(buffer_size)
                print reply2
                print "after receiving"
                if(len(reply2) > 0):
                    reply1 = reply1 + reply2
                else:
                    break
                #print reply1
                # if(len(reply1) > 0):
                #     file1.write(reply1)
                #     conn.send(reply1)
                # else:
                #     break

            conn.send(reply1)
            file1.write(reply1)
            file1.close()

        if response_code == 304:
            file = open(path, "r")
            for line in file:
                conn.send(line)
            file.close()
    except IOError:
        print "Not found in cache"
        serverReq = serverReq + "\r\n"
        s.send(serverReq)
        if bool(cacheDict):
            print "cache dict not empty, it has ", count
            minKey = min(cacheDict.iteritems(), key=operator.itemgetter(1))[0]
            print "Minkey: ", minKey
        if count < 3 or dict1[path] > cacheDict[minKey]:
            print "count < 3"
            file = open(path,"w")
            flag = 1
            if count < 3:
                cacheDict[path] = dict1[path]
            elif count == 3:
                cacheDict[path] = dict1[path]
                trash = cacheDict.pop(minKey, None)
                print "Trash: ",trash," popping: ",minKey
                try:
                    print "trying to remove"
                    print type(minKey)
                    #os.unlink(minKey)
                    os.system("rm "+minKey)
                    print "trying harder"
                except Exception, e:
                    print "couldnt print"
                    # print "Failed with:", e
                #print "After updating cache: ",cacheDict
        print "After updating cache: ",cacheDict

        while 1:
            reply = s.recv(buffer_size)
            print reply
            if(len(reply) > 0):
                conn.send(reply)
                if flag:
                    print "writing this to file 1st time: ", reply
                    file.write(reply)
            else:
                break

        if flag:
            file.close()
        if count < 3:
            count = count + 1
        print "count after caching: ", count

main()
